﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace niduc
{
    class RAID
    {
        private int clss;
        Drive[] drives;

        public RAID(int clas, int drvTpe)
        {
            clss = clas;
            if (clas == 5)
            {
                drives = new Drive[3];
                for (int i = 0; i <= 2; i++)
                    drives[i] = new Drive();
            }
            if (clas == 6)
            {
                drives = new Drive[4];
                for (int i = 0; i <= 3; i++)
                    drives[i] = new Drive();
            }
        }

        public int raidPrice()
        {
            int price = 0;
            foreach(Drive d in drives)
            {
                price += d.price;
            }
            return price;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace niduc
{
    class WebRoute
    {
        //Storage and route in GB.
        private int daily_route;
        private int storage;
        private double dynamicFactor;
        Random rn;

        public WebRoute(int Istorage)
        {
            rn = new Random();
            this.storage = Istorage;
            daily_route = (int)Math.Floor(storage * 0.6);
            dynamicFactor = daily_route / 10000;
        }

        public void endoOfDay()
        {
            daily_route += (int)Math.Floor(dynamicFactor * rn.NextDouble() * (2 - 0.1) + 0.1);
            storage += (int)Math.Floor(storage * rn.NextDouble() * (0.05 - 0.001) + 0.001);
                
        }
    }
}

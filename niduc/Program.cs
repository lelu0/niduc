﻿using niduc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NiDUC2
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

            Drive Samsung = new Drive(375, 540, 520, 1.5, 5);
            Drive IntelP45 = new Drive(1900, 3260, 1000, 2, 5);
            Drive IntelD36 = new Drive(1500, 2100, 620, 2, 5);
            Drive IntelP31 = new Drive(650, 1800, 175, 1.6, 5);
            Drive Adata = new Drive(350, 560, 520, 2, 3);
        }
    }
}

﻿using System;

namespace niduc
{
    class Drive 
    {
        Random rn; //for rand operations
        private int durability; //defines how many read-write can drive do.
        private double failFactor; //percent of chance random failure
        private bool health; //true - working, false - failure, data lost
        private int cycleCounter; //current number of r-w cycle.
        private int rCycleCounter;  //current number of read cycle.
        private int wCycleCounter;  //current number of write cycle.
        public int price { get; private set; } // in USD
        public int maxReadSpeed { get; set; } // in MB/s
        public int maxWriteSpeed { get; set; } // in MB/s
        public double mtbf { get; set; } // Mean Time Between Failures in million hours
        public int warrantyTime { get; set; }   // in years

        public Drive()
        {
            rn = new Random();
            cycleCounter = 0;
            rCycleCounter = 0;
            wCycleCounter = 0;
            health = true;
        }
        
        public Drive(int p, int r, int w, double m, int wt)
        {
            rn = new Random();
            cycleCounter = 0;
            rCycleCounter = 0;
            wCycleCounter = 0;
            health = true;
            price = p;
            maxReadSpeed = r;
            maxWriteSpeed = w;
            mtbf = m;
            warrantyTime = wt;
        }

        public void failureCalculate()
        {
            double ff = 100 / (durability - cycleCounter) * failFactor;
            double iF = rn.NextDouble() * 100;
            if (ff > iF)
                health = false;
        }

        public void WCycle()
        {
            wCycleCounter++;
            cycleCounter++;
            failureCalculate();
        }

        public void RCycle()
        {
            rCycleCounter++;
            cycleCounter++;
            failureCalculate();
        }
    }
}

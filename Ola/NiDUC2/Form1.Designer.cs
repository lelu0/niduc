﻿namespace NiDUC2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelWelcome = new System.Windows.Forms.Label();
            this.comboBoxReliability = new System.Windows.Forms.ComboBox();
            this.buttonSimulation = new System.Windows.Forms.Button();
            this.labelStorageInfo = new System.Windows.Forms.Label();
            this.textBoxStorage = new System.Windows.Forms.TextBox();
            this.labelGB = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelWelcome
            // 
            this.labelWelcome.AutoSize = true;
            this.labelWelcome.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.labelWelcome.Location = new System.Drawing.Point(12, 28);
            this.labelWelcome.Name = "labelWelcome";
            this.labelWelcome.Size = new System.Drawing.Size(327, 31);
            this.labelWelcome.TabIndex = 0;
            this.labelWelcome.Text = "Welcome to our simulator!";
            // 
            // comboBoxReliability
            // 
            this.comboBoxReliability.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F);
            this.comboBoxReliability.FormattingEnabled = true;
            this.comboBoxReliability.Location = new System.Drawing.Point(35, 118);
            this.comboBoxReliability.Name = "comboBoxReliability";
            this.comboBoxReliability.Size = new System.Drawing.Size(121, 21);
            this.comboBoxReliability.TabIndex = 1;
            this.comboBoxReliability.Text = "Choose reliability...";
            // 
            // buttonSimulation
            // 
            this.buttonSimulation.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.buttonSimulation.Location = new System.Drawing.Point(218, 127);
            this.buttonSimulation.Name = "buttonSimulation";
            this.buttonSimulation.Size = new System.Drawing.Size(121, 34);
            this.buttonSimulation.TabIndex = 2;
            this.buttonSimulation.Text = "Start simulation!";
            this.buttonSimulation.UseVisualStyleBackColor = true;
            this.buttonSimulation.Click += new System.EventHandler(this.buttonSimulation_Click);
            // 
            // labelStorageInfo
            // 
            this.labelStorageInfo.AutoSize = true;
            this.labelStorageInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.labelStorageInfo.Location = new System.Drawing.Point(15, 80);
            this.labelStorageInfo.Name = "labelStorageInfo";
            this.labelStorageInfo.Size = new System.Drawing.Size(216, 17);
            this.labelStorageInfo.TabIndex = 3;
            this.labelStorageInfo.Text = "How much storage do you need?";
            this.labelStorageInfo.Click += new System.EventHandler(this.labelStorageInfo_Click);
            // 
            // textBoxStorage
            // 
            this.textBoxStorage.Location = new System.Drawing.Point(237, 80);
            this.textBoxStorage.Name = "textBoxStorage";
            this.textBoxStorage.Size = new System.Drawing.Size(75, 20);
            this.textBoxStorage.TabIndex = 4;
            // 
            // labelGB
            // 
            this.labelGB.AutoSize = true;
            this.labelGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.labelGB.Location = new System.Drawing.Point(318, 84);
            this.labelGB.Name = "labelGB";
            this.labelGB.Size = new System.Drawing.Size(27, 16);
            this.labelGB.TabIndex = 5;
            this.labelGB.Text = "GB";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(357, 184);
            this.Controls.Add(this.labelGB);
            this.Controls.Add(this.textBoxStorage);
            this.Controls.Add(this.labelStorageInfo);
            this.Controls.Add(this.buttonSimulation);
            this.Controls.Add(this.comboBoxReliability);
            this.Controls.Add(this.labelWelcome);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelWelcome;
        private System.Windows.Forms.ComboBox comboBoxReliability;
        private System.Windows.Forms.Button buttonSimulation;
        private System.Windows.Forms.Label labelStorageInfo;
        private System.Windows.Forms.TextBox textBoxStorage;
        private System.Windows.Forms.Label labelGB;
    }
}

